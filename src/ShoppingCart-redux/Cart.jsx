import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      const { id, name, img, price } = item.product;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>
            <img style={{ width: 120 }} src={img} alt="" />
          </td>
          <td>{name}</td>
          <td>
            <button
              onClick={() => {
                this.descreaseQuantity(id);
              }}
              className="btn btn-info"
            >
              -
            </button>
            <span className="mx-3">{item.quantity}</span>
            <button
              onClick={() => this.inscreaseQuantity(id)}
              className="btn btn-info"
            >
              +
            </button>
          </td>
          <td>{price}</td>
          <td>{price * item.quantity}</td>
          <td>
            <button
              onClick={() => {
                this.delete(id);
              }}
              className="btn btn-danger"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };

  inscreaseQuantity = (id) => {
    const cloneCart = [...this.props.cart];
    const foundIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });
    cloneCart[foundIndex].quantity++;

    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: {
        cloneCart,
      },
    });
  };

  descreaseQuantity = (id) => {
    const cloneCart = [...this.props.cart];
    const foundIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });
    if (cloneCart[foundIndex].quantity > 1) cloneCart[foundIndex].quantity--;
    else if ((cloneCart[foundIndex].quantity = 1)) {
      cloneCart.splice(foundIndex, 1);
    }
    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: {
        cloneCart,
      },
    });
  };

  delete = (id) => {
    const cloneCart = [...this.props.cart];

    const foundIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });

    cloneCart.splice(foundIndex, 1);

    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: {
        cloneCart,
      },
    });
  };

  makePayment = () => {
    const cloneCart = [];
    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: {
        cloneCart,
      },
    });
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        {/* sửa sm thành xl */}
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">GIỎ HÀNG</h5>
              {/* sửa title */}
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {/* sửa body thành table */}
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã sp</th>
                    <th>hình ảnh</th>
                    <th>tên</th>
                    <th>số lượng</th>
                    <th>đơn giá</th>
                    <th>thành tiền</th>
                    <th>Xóa</th>
                  </tr>
                </thead>
                <tbody>{this.renderCart()}</tbody>
              </table>
              {/* vô home.jsx chỉnh data toggle để hiện modal */}
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button onClick={this.makePayment} type="button" className="btn btn-primary">
                Thanh toán
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.listReducer.cart,
  };
};
export default connect(mapStateToProps)(Cart);
