import React, { Component } from "react";
import { connect } from "react-redux";

class Item extends Component {
  handleSelect = () => {
    const singleItem = this.props.prod;
    this.props.dispatch({
      type: "SELECT_PRODUCT",
      payload: {
        singleItem,
      },
    });
  };

  addToCart = (toBuyProduct) => {
    const cloneCart = [...this.props.cart];
    const foundIndex = cloneCart.findIndex((item) => {
      return item.product.id === toBuyProduct.id;
    });
    if (foundIndex === -1) {
      const cartItem = { product: toBuyProduct, quantity: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[foundIndex].quantity++;
    }
    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: {
        cloneCart,
      },
    });
  };

  render() {
    const { name, img } = this.props.prod;
    return (
      <div className="card">
        <img style={{ height: 250, width: "100%" }} src={img} alt="product" />
        <div className="card-body">
          <h4>{name}</h4>
          <button onClick={this.handleSelect} className="btn btn-info">
            Chi tiết
          </button>
          <button
            onClick={() => this.addToCart(this.props.prod)}
            className=" btn btn-danger"
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.listReducer.cart,
  };
};
export default connect(mapStateToProps)(Item);
