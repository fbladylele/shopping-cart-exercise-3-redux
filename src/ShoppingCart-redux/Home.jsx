import React, { Component } from "react";
import List from "./List";
import Cart from "./Cart";
import Details from "./Details";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center">BÀI TẬP GIỎ HÀNG</h1>
        <h4
          data-toggle="modal"
          data-target="#modelId"
          className="text-center text-danger"
        >
          Giỏ hàng
        </h4>
        <List />
        {this.props.selectedProduct && <Details/>}
        
        <Cart />
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return{
    selectedProduct: state.listReducer.selectedProduct,
  }
}
export default connect(mapStateToProps) (Home);
