import React, { Component } from "react";
import Item from "./Item";
import {connect} from "react-redux";

class List extends Component {
  renderProducts = ()=>{
    return this.props.products.map((item)=>{
      return (
        <div key={item.id} className="col-3">
            <Item prod={item}/>
        </div>
      )
    })
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.renderProducts()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return {
    products: state.listReducer.productList
  }
}
export default connect(mapStateToProps) (List);
